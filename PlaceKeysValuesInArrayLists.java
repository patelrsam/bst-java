package tree;
import java.util.*;

/**
 * This task places key/values in two arrays in the order
 * in which the key/values are seen during the traversal.  If no keys/values
 * are found the ArrayList will be empty (constructor creates two
 * empty ArrayLists).  
 *
 * @param <K>
 * @param <V>
 */
public class PlaceKeysValuesInArrayLists<K,V> implements TraversalTask<K, V> {
	ArrayList<K> keyAL;
	ArrayList<V> valueAL;
	/**
	 * Creates two ArrayList objects: one for the keys and one for the values.
	 */
	public PlaceKeysValuesInArrayLists() {
		keyAL=new ArrayList<K>();
		valueAL=new ArrayList<V>();
	
	}
	
	/**
	 * Adds key/value to the corresponding ArrayLists.
	 */
	public void performTask(K key, V value) {
		keyAL.add(key);
		valueAL.add(value);
	}

	/**
	 * Returns reference to ArrayList holding keys.
	 * @return ArrayList
	 */
	public ArrayList<K> getKeys() {
		return keyAL;
	}
	
	/**
	 * Returns reference to ArrayList holding values.
	 * @return ArrayList
	 */
	public ArrayList<V> getValues() {
		return valueAL;
	}
}
