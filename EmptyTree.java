package tree;

import java.util.Collection;

/**
 * This class is used to represent the empty search tree: a search tree that
 * contains no entries.
 * 
 * This class is a singleton class: since all empty search trees are the same,
 * there is no need for multiple instances of this class. Instead, a single
 * instance of the class is created and made available through the static field
 * SINGLETON.
 * 
 * The constructor is private, preventing other code from mistakenly creating
 * additional instances of the class.
 *  
 */
 public class EmptyTree<K extends Comparable<K>,V> implements Tree<K,V> {
	/**
	 * This static field references the one and only instance of this class.
	 * We won't declare generic types for this one, so the same singleton
	 * can be used for any kind of EmptyTree.
	 */
	private static EmptyTree SINGLETON = new EmptyTree();


	public static  <K extends Comparable<K>, V> EmptyTree<K,V> getInstance() {
		return SINGLETON;
	}

	/*
	 * Constructor is private to enforce it being a singleton
	 *  
	 */
	private EmptyTree() {
		// Nothing to do
	}
	/**
	 * @return returns Null since the tree have nothing in it
	 */
	public V search(K key) {
		return null;
	}
	
	/**
	 * Inserts values passed in to the empty tree. Thus returning a new NonEmpty tree with
	 * the Key and Value passed in and SINGELTON for both left and right sub trees
	 * 
	 * @param K key - the key of the tree
	 * @param V Value - Value associated with the key
	 * 
	 * @return new NonEmpty tree with two SINGLETONS for left and right child 
	 */
	public NonEmptyTree<K, V> insert(K key, V value) {
		return new NonEmptyTree<K, V>(key, value, SINGLETON, SINGLETON);
		
	}

	/**
	 * deletes the key, since this is a empty tree, it will only return the current instance.
	 * 
	 * @return current instance of the EmptyTree
	 */
	public Tree<K, V> delete(K key) {
		return this;
	}
	
	/**
	 * Return the maximum of the tree, in Empty tree's case it will 
	 * throw an TreeIsEmptyException 
	 * 
	 * @throws TreeIsEmptyException
	 */
	public K max() throws TreeIsEmptyException {
		throw new TreeIsEmptyException();
	}
	
	/**
	 * Return the minimum of the tree, in Empty tree's case it will 
	 * throw an TreeIsEmptyException 
	 * 
	 * @throws TreeIsEmptyException
	 */
	public K min() throws TreeIsEmptyException {
		throw new TreeIsEmptyException();
	}
	
	/**
	 * Returns the size of the tree, in this case it will be 0
	 * 
	 * @return size of the tree 
	 */
	public int size() {
		return 0;
	}

	/**
	 * adds all the keys to a collection, for EmptyTree it will do nothing
	 * 
	 * @param Collection type K 
	 */
	public void addKeysToCollection(Collection<K> c) {
		return;
	}
	
	/**
	 * Creates and returns a sub tree form the specified range
	 * For EmptyTree it will do nothing
	 * 
	 * @param K fromKey - the minimum range of the subtree
	 * @param K toKey - the maximum range of the subtree
	 */
	public Tree<K,V> subTree(K fromKey, K toKey) {
		
		return this;
	}
	
	/**
	 * Returns the Height of the tree, for EmptyTree it will always be 0
	 * 
	 * @return height of the EmptyTree
	 */
	public int height() {
		return 0;
	}

	/**
	 * Performs TraversalTask with inorder-Traversal 
	 * 
	 *  @param TraversalTask type K,V
	 */
	public void inorderTraversal(TraversalTask<K,V> p) {
		return;
	}
	/**
	 * Performs TraversalTask with rightRootLeft-Traversal, 
	 * this is essentially backwards inorder-Traversal  
	 * 
	 *  @param TraversalTask type K,V
	 */
	public void rightRootLeftTraversal(TraversalTask<K,V> p) {
		return;
	}
}