package tree;

import java.util.Collection;

import org.junit.Test.None;

/**
 * This class represents a non-empty search tree. An instance of this class
 * should contain:
 * <ul>
 * <li>A key
 * <li>A value (that the key maps to)
 * <li>A reference to a left Tree that contains key:value pairs such that the
 * keys in the left Tree are less than the key stored in this tree node.
 * <li>A reference to a right Tree that contains key:value pairs such that the
 * keys in the right Tree are greater than the key stored in this tree node.
 * </ul>
 * 
 */
public class NonEmptyTree<K extends Comparable<K>, V> implements Tree<K, V> {
	private K key;
	private V value;
	private Tree<K, V> left, right;

	/* Provide whatever instance variables you need */

	/**
	 * Only constructor we need.
	 * 
	 * @param key
	 * @param value
	 * @param left
	 * @param right
	 */
	public NonEmptyTree(K key, V value, Tree<K, V> left, Tree<K, V> right) {
		this.key = key;
		this.value = value;
		this.left = left;
		this.right = right;

	}

	/**
	 * Searches the tree for the Key and returns the value associated with it
	 * 
	 * @param K
	 *            Key - the Key that is associated with a value
	 * 
	 * @return value that is associated with the key
	 */

	public V search(K key) {
		int comp = key.compareTo(this.key);

		if (comp == 0) {
			return this.value;
		} else if (comp < 0) {
			return this.left.search(key);
		} else {
			return this.right.search(key);
		}
	}

	/**
	 * Inserts a new key and value to the tree, if the Key already exists then
	 * the value associated with if will be updated.
	 * 
	 * @param K
	 *            key - Key for the value
	 * @param V
	 *            Value - Value
	 * 
	 * @return updated tree
	 */
	public NonEmptyTree<K, V> insert(K key, V value) {
		int comp = key.compareTo(this.key);
		if (comp == 0) {
			this.value = value;
		} else if (comp > 0) {
			right = right.insert(key, value);
		} else {
			left = left.insert(key, value);
		}
		return this;
	}

	/**
	 * Deletes the particular key from the tree. 
	 * If the Key does not exist then no changes will be done.
	 * 
	 * @param K
	 *            Key - Key that is to be deleted
	 * @return Updated tree
	 */
	public Tree<K, V> delete(K key) {
		int comp = key.compareTo(this.key);
		if (comp < 0) {
			left = left.delete(key);
		}

		else if (comp > 0) {
			right = right.delete(key);
		}

		else {
			try {
				K tempKey = left.max();
				V tempVal = left.search(tempKey);
				this.key = tempKey;
				this.value = tempVal;
				this.left = left.delete(this.key);
			} catch (TreeIsEmptyException e) { 
				// if there is no left child to the tree
				try {
					K tempKeyR = right.min();
					V tempValR = right.search(tempKeyR);
					this.key = tempKeyR;
					this.value = tempValR;
					this.right = right.delete(this.key);
				} catch (TreeIsEmptyException e1) { 
					// if there is no left or right tree then EmptyTree is returned
					return EmptyTree.getInstance();
				}
			}
		}

		return this;
	}

	/**
	 * Return the Maximum key of the tree
	 * 
	 * @return Maximum key
	 */
	public K max() {
		try {
			return this.right.max();
		} catch (TreeIsEmptyException e) {
			return this.key;
		}
	}

	/**
	 * Return the Minimum key of the tree
	 * 
	 * @return Minimum key
	 */
	public K min() {
		try {
			return this.left.min();
		} catch (TreeIsEmptyException e) {
			return this.key;
		}
	}

	/**
	 * Returns the size of the tree
	 * 
	 * @return size of the tree
	 */
	public int size() {
		return 1 + left.size() + right.size();
	}

	/**
	 * adds all the keys to a Collection
	 * 
	 * @param Collection type K
	 */
	public void addKeysToCollection(Collection<K> c) {
		left.addKeysToCollection(c);
		c.add(key);
		right.addKeysToCollection(c);
	}

	/**
	 * Creates and returns a sub tree form the specified range
	 * 
	 * @param K
	 *            fromKey - the minimum range of the subtree
	 * @param K
	 *            toKey - the maximum range of the subtree
	 */
	public Tree<K, V> subTree(K fromKey, K toKey) {
		int compFrom = this.key.compareTo(fromKey);
		int compTo = this.key.compareTo(toKey);

		if (compFrom < 0) {
			return right.subTree(fromKey, toKey);
		} else if (compTo > 0) {
			return left.subTree(fromKey, toKey);
		} else {
			return new NonEmptyTree<K, V>(key, value, left.subTree(fromKey,
					toKey), right.subTree(fromKey, toKey));
		}
	}

	/**
	 * Returns the Height of the tree
	 * 
	 * @return height of the EmptyTree
	 */
	public int height() {
		int leftLength = left.size();
		int rightLength = right.size();

		return (leftLength > rightLength ? 1 + left.height() : 1 + right
				.height());
	}

	/**
	 * Performs TraversalTask with inorder-Traversal
	 * 
	 * @param TraversalTask type K,V
	 */
	public void inorderTraversal(TraversalTask<K, V> p) {
		left.inorderTraversal(p);
		p.performTask(key, value);
		right.inorderTraversal(p);
	}

	/**
	 * Performs TraversalTask with rightRootLeft-Traversal, this is essentially
	 * backwards inorder-Traversal
	 * 
	 * @param TraversalTask
	 *            type K,V
	 */
	public void rightRootLeftTraversal(TraversalTask<K, V> p) {
		right.rightRootLeftTraversal(p);
		p.performTask(key, value);
		left.rightRootLeftTraversal(p);
	}

}